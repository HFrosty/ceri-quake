#! /bin/sh
SETUP_PRODUCTNAME="ioquake3"
SETUP_PRODUCTVER="1.32"
SETUP_COMPONENTNAME="Default"
SETUP_COMPONENTVER="1.36"
SETUP_INSTALLPATH="/home/nas02a/etudiants/inf/uapv1903831/ioquake3"
SETUP_SYMLINKSPATH="/home/nas02a/etudiants/inf/uapv1903831/.local/bin"
SETUP_CDROMPATH="(null)"
SETUP_DISTRO="debian"
SETUP_OPTIONTAGS=""
SETUP_ARCH="x86_64"
export SETUP_PRODUCTNAME SETUP_PRODUCTVER SETUP_COMPONENTNAME SETUP_COMPONENTVER
export SETUP_INSTALLPATH SETUP_SYMLINKSPATH SETUP_CDROMPATH SETUP_DISTRO SETUP_OPTIONTAGS SETUP_ARCH
#!/bin/sh
NAME="ioquake3-q3a"
DESKTOPFILES="
ioquake3-q3ta.desktop
"
xdg_desktop_menu=`which xdg-desktop-menu 2>/dev/null`
if test "x$xdg_desktop_menu" = x; then
  xdg_desktop_menu=./xdg-desktop-menu-"$NAME"
fi

for i in $DESKTOPFILES; do
  test -e "$SETUP_INSTALLPATH"/"$i".in || continue
  $xdg_desktop_menu uninstall --novendor "$i"
  rm "$i" || true
done
